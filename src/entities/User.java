
package entities;


public class User {
    
    private int idUser;
    private String fullName;
    private String genreUser;
    private String email;
    private String mdp;
    
    private String region;
    private String municipalite;
    private String telephone;
    private String role;
  
    
public User(){
        
    }
    public User(int idUser, String fullName, String genreUser, String email, String mdp, String region, String municipalite, String telephone, String role) {
        this.idUser = idUser;
        this.fullName = fullName;
        this.genreUser = genreUser;
        this.email = email;
        this.mdp = mdp;
        this.region = region;
        this.municipalite = municipalite;
        this.telephone = telephone;
        this.role = role;
      
    }

    public User(String fullName, String genreUser, String email, String mdp, String region, String municipalite, String telephone, String role) {
        this.fullName = fullName;
        this.genreUser = genreUser;
        this.email = email;
        this.mdp = mdp;
        this.region = region;
        this.municipalite = municipalite;
        this.telephone = telephone;
        this.role = role;
      
    }

    
    


    

    public int getIdUser() {
        return idUser;
    }

    public String getFullName() {
        return fullName;
    }

    public String getGenreUser() {
        return genreUser;
    }

    public String getEmail() {
        return email;
    }

    public String getMdp() {
        return mdp;
    }

    public String getRegion() {
        return region;
    }

    public String getMunicipalite() {
        return municipalite;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getRole() {
        return role;
    }

 

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setGenreUser(String genreUser) {
        this.genreUser = genreUser;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setMunicipalite(String municipalite) {
        this.municipalite = municipalite;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "idUser=" + idUser + ", fullName=" + fullName + ", genreUser=" + genreUser + ", email=" + email + ", mdp=" + mdp + ", region=" + region + ", municipalite=" + municipalite + ", telephone=" + telephone + ", role=" + role + '}';
    }


  
    
}


    

