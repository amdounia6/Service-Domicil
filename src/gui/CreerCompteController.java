
package gui;

import entities.User;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import services.UserCRUD;


public class CreerCompteController implements Initializable {

    
    @FXML
    private Button valider;
    @FXML
    private Button Connecter;
    @FXML
    private TextField id_fullName;
    @FXML
    private TextField id_region;
    @FXML
    private TextField id_mdp;
    @FXML
    private TextField id_telephone;
    @FXML
    private TextField id_jourTravail;
    @FXML
    private TextField id_heureTravail;
    @FXML
    private TextField id_adresseAgence;
    @FXML
    private TextField id_email;
    @FXML
    private TextField id_municipalite;
    @FXML
    private ComboBox id_role;
    @FXML
    private ComboBox id_genre;
    private Parent fxml;
    private Scene scene;
    private Stage stage;
    @FXML
    private Button id_dashboard;
    @FXML
    private Button id_home;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> list = FXCollections.observableArrayList("User" , "Employer" ) ;
        id_role.setItems(list);
        ObservableList<String> liste = FXCollections.observableArrayList("Femme" , "Homme") ;
        id_genre.setItems(liste);
    }    
    
          public Connection getConnection(){ 
          
          Connection conx ;
          try{
              conx = DriverManager.getConnection("\"jdbc:mysql://localhost:3306/ServiceDomicile") ;
              return conx ;
          }catch(SQLException ex){
              System.out.println("Error: "+ ex.getMessage());
              return null ;
          }
          
      }



    @FXML
    private void valider(ActionEvent event) {
       
    String fullName;
    String genreUser;
    String email;
    String mdp;
    String region;
     String municipalite;
    String telephone;
     String role;
    
     
     fullName=id_fullName.getText();
     genreUser=id_genre.getSelectionModel().getSelectedItem().toString(); 
     email=id_email.getText();
     mdp=id_mdp.getText();
     region=id_region.getText();
     municipalite=id_municipalite.getText();
     telephone=id_telephone.getText();
     role=id_role.getSelectionModel().getSelectedItem().toString(); 
     
     if(id_fullName.getText().isEmpty() || id_email.getText().isEmpty() ||id_mdp.getText().isEmpty() || id_region.getText().isEmpty() || id_municipalite.getText().isEmpty() || id_telephone.getText().isEmpty() || id_adresseAgence.getText().isEmpty() || id_jourTravail.getText().isEmpty() || id_heureTravail.getText().isEmpty())
     {
             Alert alert = new Alert(Alert.AlertType.ERROR);
             alert.setHeaderText("Veuillez remplir tous les champs");
             alert.showAndWait();
     }
     else
        {
            User U1 = new User (fullName,genreUser,email,mdp,region,municipalite,telephone,role);
            UserCRUD uc= new UserCRUD () ;
            uc.ajouterUser(U1);
        
        JOptionPane.showMessageDialog(null,"Succés de création ");
        }
    }

 @FXML
    private void butt_conn(ActionEvent event) throws IOException {
             FXMLLoader loader = new FXMLLoader(getClass().getResource("Authentification.fxml"));
             fxml=loader.load(); 
             stage=(Stage)((Node)event.getSource()).getScene().getWindow();
             scene=new Scene((Parent) fxml);
             stage.setScene(scene);
             stage.centerOnScreen();
             stage.show(); 
    }

    @FXML
    private void dashboard(ActionEvent event)throws IOException {
             FXMLLoader loader = new FXMLLoader(getClass().getResource("Dashboard.fxml"));
             fxml=loader.load(); 
             stage=(Stage)((Node)event.getSource()).getScene().getWindow();
             scene=new Scene((Parent) fxml);
             stage.setScene(scene);
             stage.centerOnScreen();
             stage.show(); 
        
    }

    @FXML
    private void home(ActionEvent event) throws IOException{
             FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
             fxml=loader.load(); 
             stage=(Stage)((Node)event.getSource()).getScene().getWindow();
             scene=new Scene((Parent) fxml);
             stage.setScene(scene);
             stage.centerOnScreen();
             stage.show(); 
    }
}
    

   
    

