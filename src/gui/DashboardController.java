
package gui;

import entities.User;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import services.UserCRUD;


public class DashboardController implements Initializable {

    @FXML
    private Button id_listeUSer;
    @FXML
    private Button id_deconn_admin;
    @FXML
    private TableView<User> id_liste;
    @FXML
    private TableColumn<User, Integer> id_idUser;
    @FXML
    private TableColumn<User, String> id_fullName;
    @FXML
    private TableColumn<User, String> id_genreUSer;
    @FXML
    private TableColumn<User, String> id_email;
    @FXML
    private TableColumn<User, String> id_mdp;
    @FXML
    private TableColumn<User, String> id_region;
    @FXML
    private TableColumn<User, String> id_municipalite;
    @FXML
    private TableColumn<User, String> id_telephone;
    @FXML
    private TableColumn<User, String> id_role;
    
    private Parent fxml;
    
    private Scene scene;
    
    private Stage stage;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void listeUser(ActionEvent event) {
        UserCRUD uc =new UserCRUD();
        ArrayList arrayList = null;
        try {
            arrayList = (ArrayList) uc.selectAll();
        } catch (SQLException ex) {
            Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList observableList = FXCollections.observableArrayList(arrayList);
        id_liste.setItems(observableList);
        id_idUser.setCellValueFactory(new PropertyValueFactory<>("idUser"));
        id_fullName.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        id_genreUSer.setCellValueFactory(new PropertyValueFactory<>("genreUser"));
        id_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        id_mdp.setCellValueFactory(new PropertyValueFactory<>("mdp"));
        id_region.setCellValueFactory(new PropertyValueFactory<>("region"));
        id_municipalite.setCellValueFactory(new PropertyValueFactory<>("municipalite"));
        id_telephone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
        id_role.setCellValueFactory(new PropertyValueFactory<>("role"));
    }

    @FXML
    private void deconn_admin(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Authentification.fxml"));
             fxml=loader.load(); 
             stage=(Stage)((Node)event.getSource()).getScene().getWindow();
             scene=new Scene((Parent) fxml);
             stage.setScene(scene);
             stage.centerOnScreen();
             stage.show(); 
        
    }
    
}
