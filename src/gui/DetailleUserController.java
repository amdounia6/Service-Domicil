/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package gui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author mahdi
 */


public class DetailleUserController implements Initializable {

    @FXML
    private String lbNom;

    @FXML
    private String lbPrenom;
    /**
     * Initializes the controller class.
     * @param url
     */
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public String getLbNom() {
        return lbNom;
    }

    public String getLbPrenom() {
        return lbPrenom;
    }

   /* public void setLbNom(Label lbNom) {
        this.lbNom = lbNom;
    }

    public void setLbPrenom(Label lbPrenom) {
        this.lbPrenom = lbPrenom;
    }*/


    public void setLbNom(String lbNom) {
        this.lbNom=lbNom;
    }

    public void setLbPrenom(String lbPrenom) {
        this.lbPrenom=lbPrenom;
    }
}
