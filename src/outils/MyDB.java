/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package outils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mahdi
 */
//cette class va faire cnx a la BD
public class MyDB {
    //On a besoin de 3 attribue qui represente parametre de mon BD
      //url="Type de API qu'on va utilser:Type de la SGBD://si local je met localhost si notre BD est dans un serveur je met @IP du serveur/le nom de notre BD"
    String url="jdbc:mysql://localhost/4se4";
      //par défaul root
    String user="root";
      //par défaul vide si on utilse un mot passe pour connecter a PhpMyadmin on doit le mettre ici
    String password="";
      //Creer un variable de type Connection enfaite on plusieur type de class et d'interface Connection ce que on va utilser est java.sql
      //Cette interface va nous permettre d'etablire la connexion
    Connection cnx;
    
    
    
    //Mainetanant on doit inisialiser pour insialiser les attribut generalement  au sein du constructeur
    static MyDB instance;//Attribut static le meme pour dans les class et pour tous les objet
    //1 creer constructeur par défaut dans ce construcetru on doit initialiser cnx
    //RQ:DriverManager.getConnection cad getConnection METHODE statique cad meme methode on peut l'utilser avec tous les objets sans l'instancier
    //il y a 3 Methode getConnection on va utilser Methode avec 3 parametre 
    private MyDB(){
        //cette inscription peut genere un exception wrong password or username
        try {
            cnx= DriverManager.getConnection(url,user,password);//java methode statique & DriverManager va essayer d'etablire la cnx avec la BD
            System.out.println("connexion établie");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        // new User().getPrenom(); non statique!!
    }
    
    //Getter du methode instance avec une condtion afin d'd'avoire est ce que variable est déja intialiser ou pas 
    public static MyDB getInstance(){
            if (instance==null){
                instance =new MyDB();
            }
            return instance;
        }
    
    //Car j'ai besoin de ce attribut pour pouvoire executer mon requete dans Userservice
    public Connection getCnx() {
        return cnx;
    }
    
    
}
