package services;

import entities.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utils.MyDB;



public class UserCRUD implements InterfaceUser{
    Connection conx = MyDB.getInstance().getConnection();
    private Object connexion;

    @Override
    public void ajouterUser(User U) {
             String req ="INSERT INTO `user`(`fullName`,`genreUser`,`email`,`mdp`,`region`,`municipalite`,`telephone`,`role`)VALUES ('"+U.getFullName()+"','" +U.getGenreUser()+"','"+U.getEmail()+"','"+U.getMdp()+"','"+U.getRegion()+"','"+U.getMunicipalite()+"','"+U.getTelephone()+"','"+U.getRole()+"')";
              Statement st ;
              try {
              st = conx.createStatement() ;
              st.executeUpdate(req) ;
              System.out.println("L'utilisateur est ajouté");}
              
               catch (SQLException ex) {
                System.err.println(ex.getMessage());
        }
        
    }
        @Override
    public void supprimerUser(int idUser) {

    try {
        String req=" DELETE FROM user WHERE idUser="+ idUser ;

        PreparedStatement St = conx.prepareStatement(req);
        St.executeUpdate();
        System.out.println("L'utilisateur est supprimé");}
     catch (SQLException ex) {
        System.err.println(ex.getMessage());
    }

    }
    @Override
    public void modifierUser(User U ,int idUser) {
       String req = "UPDATE `user` SET `fullName`='"+U.getFullName()+"',`genreUser`='"+U.getGenreUser()+"',`email`='"+U.getEmail()+"',`mdp`='"+U.getMdp()+"',`region`='"+U.getRegion()+"',`municipalite`='"+U.getMunicipalite()+"',`telephone`='"+U.getTelephone()+"',`role`='"+U.getRole()+"' WHERE idUser ="+U.getIdUser() ;
        Statement st  ;
        try {
        st = conx.createStatement() ;
        st.executeUpdate(req) ;
        System.out.println("L'utilisateur est modifié");}
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    @Override
    public List<User> Afficher() {

    List <User> list = new ArrayList<>();
        try {
    String req = "SELECT * FROM  User";        
    Statement st;
    st =conx.createStatement();
    ResultSet rs = st.executeQuery(req);
    System.out.println("L'utilisateur est affiché");
    while(rs.next()){
    User u = new User (rs.getInt(1), rs.getString("fullname"),rs.getString("genreUser"), rs.getString("email"),rs.getString("mdp"),rs.getString("region"),rs.getString("municipalite"),rs.getString("telephone"),rs.getString("role"));
    list.add(u);
              }
        }
        catch (SQLException ex){
    System.err.println(ex.getMessage());
}
    return list;
    }  

 public boolean verifierLogin(String email,String password)throws SQLException{
    String req="SELECT * FROM `utilisateur` WHERE email=? AND mdp=? ;";
            int id=-1;
            PreparedStatement pst =conx.prepareStatement(req);
            pst.setString(1,email);
            pst.setString(2,password);            
            ResultSet rs=pst.executeQuery();
            while(rs.next())
            {
                id=rs.getInt(1);
                System.out.println(id);
            }
            if(id!=-1)
                return true;
            
            return false;
    }

   /* public User getUser(String email)throws SQLException {
        User U=new User();
    
    String req="SELECT * FROM `user` WHERE email=?  ;";
           
            PreparedStatement pst =conx.prepareStatement(req);
            pst.setString(1,email);

            ResultSet rs=pst.executeQuery();
            while(rs.next())
            {
               U.setIdUSer(rs.getInt(1));
               u.setNom(rs.getString(2));
               u.setPrenom(rs.getString(3));
               u.setDate_naiss(rs.getString(4));
               u.setEmail(rs.getString(5));
               u.setTel(rs.getString(6));
               u.setAdresse(rs.getString(7));
               u.setNom_utilisateur(rs.getString(8));
               u.setMot_de_passe(rs.getString(9));
               u.setSexe(rs.getString(10));
               u.setPhoto_de_profil(rs.getString(11));
               u.setMtc(rs.getString(12));
               u.setRole(rs.getString(13));
               u.setRank(rs.getString(14));
               u.setId_consommateur(rs.getString(15));
               u.setCode(rs.getInt(16));

            }
    return u;
    }
    }*/
    public  List<User> selectAll() throws SQLException {
        //LIST
        List<User> arrayList = new ArrayList<>();
        //request 
        String req ="SELECT * FROM user ";

            //insert
            Statement st = conx.createStatement();
            ResultSet rs = st.executeQuery(req);
            while(rs.next())
            {
                arrayList.add(new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9)));
                
            }
          
        return arrayList;

    }

    @Override
    public void supprimerUser_home(String fullName) {
        try {
        String req=" DELETE FROM user WHERE fullName=?" ;

        PreparedStatement St = conx.prepareStatement(req);
        St.setString(1, fullName);
        St.executeUpdate();
        System.out.println("L'utilisateur est supprimé");}
     catch (SQLException ex) {
        System.err.println(ex.getMessage());
    }
    }
}

    

