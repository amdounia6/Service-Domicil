
package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MyDB {
    final String url = "jdbc:mysql://localhost:3306/ServiceDomicile";
    final String login = "root";
    final String password = "";
    private Connection connexion;
    private static MyDB instance;

    private MyDB() {
        try {
            connexion
                    = DriverManager.getConnection(url, login, password);
            System.out.println("Connexion établie");
        } catch (SQLException ex) {
            System.out.println("Erreur de connexion à la base de données");
        }
    }
    
    //créer une instance de la bdd
    public static  MyDB getInstance(){
        if(instance == null)
            instance = new MyDB();
        return instance;
    }
    
    public Connection getConnection(){
        return getConnexion();
    }

    /**
     * @param aInstance the instance to set
     */
    public static void setInstance(MyDB aInstance) {
        instance = aInstance;
    }

    /**
     * @return the connexion
     */
    public Connection getConnexion() {
        return connexion;
    }

    /**
     * @param connexion the connexion to set
     */
    public void setConnexion(Connection connexion) {
        this.connexion = connexion;
    }
    
}
